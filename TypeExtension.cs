﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.Foundation.Security;
namespace System
{
    public static class TypeExtension
    {

        public static string ToShortDateStr(this DateTime datetime)
        {
            return datetime.ToString("yyyy-MM-dd");
        }

        public static string ToShortDateStr(this DateTime? datetime)
        {
            if (datetime.HasValue)
                return datetime.Value.ToString("yyyy-MM-dd");
            return string.Empty;
        }

        public static string ToLongDateStr(this DateTime datetime)
        {
            return datetime.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string ToLongDateStr(this DateTime? datetime)
        {
            if (datetime.HasValue)
                return datetime.Value.ToString("yyyy-MM-dd HH:mm:ss");
            return string.Empty;
        }

        public static T ToEnum<T>(this string value)
        {
            T ret;
            ret = (T)Enum.Parse(typeof(T), value, true);
            return ret;
        }

        public static T ToEnum<T>(this int value)
        {
            T ret;
            ret = (T)Enum.Parse(typeof(T), value.ToString(), true);
            return ret;
        }

        public static T ToEnum<T>(this short value)
        {
            T ret;
            ret = (T)Enum.Parse(typeof(T), value.ToString(), true);
            return ret;
        }

        public static bool ToBool(this object obj)
        {
            if (obj == null || obj is DBNull || obj.ToString() == string.Empty)
            {
                return false;
            }
            else
            {
                int retInt;
                if (int.TryParse(obj.ToString(), out retInt))
                {
                    return Convert.ToBoolean(retInt);
                };
                bool ret = false;
                bool.TryParse(obj.ToString(), out ret);
                return ret;
            }
        }

        public static int ToInt(this bool value)
        {
            if (value) return 1;
            else return 0;

        }

        public static Int16 ToInt16(this bool value)
        {
            if (value) return 1;
            else return 0;

        }

        public static long ToLong(this bool value)
        {
            if (value) return 1;
            else return 0;
        }

        public static int ToInt(this Enum value)
        {
            return Convert.ToInt32(value);
        }

        public static Int16 ToInt16(this Enum value)
        {
            return Convert.ToInt16(value);
        }

        public static long ToLong(this Enum value)
        {
            return Convert.ToInt64(value);
        }

        public static int ToInt(this string value)
        {
            return Convert.ToInt32(value);
        }

        public static Int16 ToInt16(this string value)
        {
            return Convert.ToInt16(value);
        }

        public static long ToLong(this string value)
        {
            return Convert.ToInt64(value);
        }

        public static int ToInt(this double value)
        {
            return Convert.ToInt32(value);
        }


        public static decimal ToDecimal(this float value, int precision)
        {
            return Decimal.Round(Convert.ToDecimal(value), precision);
        }

        public static decimal ToDecimal(this double value, int precision)
        {
            return Decimal.Round(Convert.ToDecimal(value), precision);
        }

        public static DateTime ToDateTime(this string value)
        {
            return Convert.ToDateTime(value);
        }


        public static string SubStr(this string value, int startIndex, int length)
        {
            var str = new StringBuilder();
            var index = 0;
            var currentLength = 1;
            str.Append("");
            foreach (var item in value)
            {
                if (index == startIndex)
                {
                    if (currentLength <= length)
                    {
                        str.Append(item);
                        startIndex++;
                        currentLength++;
                    }
                    else
                    {
                        break;
                    }
                }
                index++;
            }
            return str.ToString();
        }

        public static string DESEncrypt(this string value, string key = "d@1$#A!*")
        {
            return DES.Encrypt(value.ToString(), key);
        }

        public static string DESDecrypt(this string value, string key = "d@1$#A!*")
        {
            return DES.Decrypt(value.ToString(), key);
        }

        public static string DESEncrypt(this int value, string key = "d@1$#A!*")
        {
            return DES.Encrypt(value.ToString(), key);
        }

        public static string DESDecrypt(this int value, string key = "d@1$#A!*")
        {
            return DES.Decrypt(value.ToString(), key);
        }

        public static string DESEncrypt(this bool value, string key = "d@1$#A!*")
        {
            return DES.Encrypt(value.ToString(), key);
        }

        public static string DESDecrypt(this bool value, string key = "d@1$#A!*")
        {
            return DES.Decrypt(value.ToString(), key);
        }

        public static string ToUniqueStr(this DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssfffffff");
        }

        public static string ToUniqueStr(this DateTime? value)
        {
            if (value == null) return "";
            else
                return ToUniqueStr(value.Value);
        }



    }
}
