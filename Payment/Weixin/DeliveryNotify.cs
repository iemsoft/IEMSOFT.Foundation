﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
namespace IEMSOFT.Foundation.Payment.Weixin
{
    public class DeliveryNotify
    {
        public string appid { get; set; }
        public string openid { get; set; }
        public string transid { get; set; }
        public string out_trade_no { get; set; }
        public string deliver_timestamp { get; set; }
        public string deliver_status { get; set; }
        public string deliver_msg { get; set; }
        public string app_signature
        {
            get
            {
                _requestHandler.setParameter("appid", appid);
                _requestHandler.setParameter("openid", openid);
                _requestHandler.setParameter("transid", transid);
                _requestHandler.setParameter("out_trade_no", out_trade_no);
                _requestHandler.setParameter("deliver_timestamp", deliver_timestamp);
                _requestHandler.setParameter("deliver_status", deliver_status);
                _requestHandler.setParameter("deliver_msg", deliver_msg);
                var sign = _requestHandler.createSHA1Sign();
                return sign;
            }
        }
        public string sign_method { get; set; }
        private string _apiUrl = "https://api.weixin.qq.com/pay/delivernotify";
        private string _accessToken;
        private RequestHandler _requestHandler;
        public DeliveryNotify(string accessToken, RequestHandler requestHandler)
        {
            var builder = new StringBuilder();
            builder.Append(_apiUrl);
            builder.AppendFormat("?access_token={0}",accessToken);
            _apiUrl = builder.ToString();
            _requestHandler = requestHandler;
            _accessToken = accessToken;
        }

        public DeliveryNotify(string accessToken, string apiURL, RequestHandler requestHandler)
        {
            var builder = new StringBuilder();
            builder.Append(apiURL);
            builder.AppendFormat("?access_token={0}", accessToken);
            _apiUrl = builder.ToString();
            _requestHandler = requestHandler;
            _accessToken = accessToken;
        }

        public DeliveryNotifyResponse Notify()
        {
            var ret = new DeliveryNotifyResponse();
            try
            {
                using (var client = new HttpClient())
                {
                    var str = Newtonsoft.Json.JsonConvert.SerializeObject(this);
                    var json = new StringContent(str, Encoding.UTF8, "application/json");
                    var task = client.PostAsync(_apiUrl, json);
                    task.Result.EnsureSuccessStatusCode();
                    var result = task.Result.Content.ReadAsStringAsync();
                    var responseStr = result.Result;
                    ret = Newtonsoft.Json.JsonConvert.DeserializeObject<DeliveryNotifyResponse>(responseStr);
                }
                
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException.InnerException != null)
                {
                    ret.errmsg = ex.InnerException.InnerException.Message;
                }
                else
                {
                    ret.errmsg = ex.InnerException.Message;
                }
            }
            catch (HttpRequestException ex)
            {
                ret.errmsg = ex.Message;
            }

            catch (Exception ex)
            {
                ret.errmsg = ex.Message;        
            }
            return ret;
        }
    }

    public class DeliveryNotifyResponse
    {
        public string errcode { get; set; }
        public string errmsg { get; set; }
    }
}
