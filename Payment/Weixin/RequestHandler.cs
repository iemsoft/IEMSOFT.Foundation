﻿using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Xml;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using IEMSOFT.Foundation.Security;
namespace IEMSOFT.Foundation.Payment.Weixin
{
    public class RequestHandler
	{
    
        private string partnerKey;

		protected Hashtable parameters=new Hashtable();
		
		private string debugInfo;
		
		public virtual void init() 
		{
		}

		public String getDebugInfo() 
		{
			return debugInfo;
		}

        public String getPartnerKey() 
		{
            return partnerKey;
		}

		public void setPartnerKey(string key) 
		{
            this.partnerKey = key;
		}

        public void setParameter(string parameter, string parameterValue)
        {
            if (parameter != null && parameter != "")
            {
                if (parameters.Contains(parameter))
                {
                    parameters.Remove(parameter);
                }

                parameters.Add(parameter, parameterValue);
            }
        }

        public string getRequestURL()
        {
            this.createSign();
            StringBuilder sb = new StringBuilder();
            ArrayList akeys=new ArrayList(parameters.Keys); 
            akeys.Sort();
            foreach(string k in akeys)
            {
                string v = (string)parameters[k];
                if(null != v && "key".CompareTo(k) != 0) 
                {
                    sb.Append(k + "=" + TenpayUtil.UrlEncode(v, getCharset()) + "&");
                }
            }

            if(sb.Length > 0)
            {
                sb.Remove(sb.Length-1, 1);
            }

         
           return sb.ToString();
           
        }  
		
        protected virtual void  createSign() 
        {
            StringBuilder sb = new StringBuilder();

            ArrayList akeys=new ArrayList(parameters.Keys); 
            akeys.Sort();

            foreach(string k in akeys)
            {
                string v = (string)parameters[k];
                if(null != v && "".CompareTo(v) != 0
                    && "sign".CompareTo(k) != 0 && "key".CompareTo(k) != 0) 
                {
                    sb.Append(k + "=" + v + "&");
                }
            }

            sb.Append("key=" + this.getPartnerKey());    
          
            string sign = MD5Util.GetMD5(sb.ToString(), getCharset()).ToUpper();

            this.setParameter("sign", sign);

            this.setDebugInfo(sb.ToString() + " => sign:" + sign);		
        }
     
        public virtual string createMd5Sign()
        {
            StringBuilder sb = new StringBuilder();
            ArrayList akeys=new ArrayList(parameters.Keys); 
            akeys.Sort();

            foreach(string k in akeys)
            {
                string v = (string)parameters[k];
                if(null != v && "".CompareTo(v) != 0
                    && "sign".CompareTo(k) != 0 && "".CompareTo(v) != 0) 
                {
                    sb.Append(k + "=" + v + "&");
                }
            }
            string sign = MD5Util.GetMD5(sb.ToString(), getCharset()).ToLower();

            this.setParameter("sign", sign);
            return sign;
    }

        public string createSHA1Sign()
        {
            StringBuilder sb = new StringBuilder();
            ArrayList akeys = new ArrayList(parameters.Keys);
            akeys.Sort();

            foreach (string k in akeys)
            {
                string v = (string)parameters[k];
              if (null != v && "".CompareTo(v) != 0
                     && "sign".CompareTo(k) != 0 && "key".CompareTo(k) != 0)
                {
                    if(sb.Length==0)
                    {
                    sb.Append(k + "=" + v);
                    }
                    else{
                     sb.Append("&" + k + "=" + v);
                    }
                }
            }
            string paySign = SHA1Util.getSha1(sb.ToString(),getCharset()).ToString().ToLower();
       
            this.setDebugInfo(sb.ToString() + " => sign:" + paySign);
            return paySign;
        }

        public string parseXML()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<xml>");
            foreach (string k in parameters.Keys)
            {
                string v = (string)parameters[k];
                if (Regex.IsMatch(v, @"^[0-9.]$"))
                {

                    sb.Append("<" + k + ">" + v + "</" + k + ">");
                }
                else
                {
                    sb.Append("<" + k + "><![CDATA[" + v + "]]></" + k + ">");
                }

           }
            sb.Append("</xml>");
            return sb.ToString();
        }

		public void setDebugInfo(String debugInfo) 
		{
			this.debugInfo = debugInfo;
		}

		public Hashtable getAllParameters()
		{
			return this.parameters;
		}

        private string _charset = "UTF-8";
        public virtual void setCharset(string value)
        {
            // return this.httpContext.Request.ContentEncoding.BodyName;
            _charset = value;
        } 

         public virtual string getCharset()
      {
         // return this.httpContext.Request.ContentEncoding.BodyName;
          return _charset;
      } 
    }
}
