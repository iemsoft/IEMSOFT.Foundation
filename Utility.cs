﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IEMSOFT.Foundation
{
    public sealed class Utility
    {
        /// <summary>
        /// 获取appsetting值
        /// </summary>
        /// <typeparam name="T">要转换的类型</typeparam>
        /// <param name="defaultValue">默认值</param>
        /// <param name="key">key</param>
        /// <returns>appsetting值</returns>
        public static T GetAppSetting<T>(T defaultValue, string key)
        {
            string value = ConfigurationManager.AppSettings[key];
            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    defaultValue = (T)Convert.ChangeType(value, typeof(T));
                }
                catch
                {
                }
            }
            return defaultValue;
        }

        /// <summary>
        ///判断输入的字符串只包含数字     
        ///可以匹配整数和浮点数 ^-?\d+$|^(-?\d+)(\.\d+)?$
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNumber(string input)
        {
            string pattern = "^-?\\d+$|^(-?\\d+)(\\.\\d+)?$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(input);
        }

        public static bool IsInteger(string input)
        {
            string pattern = "^d+$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(input);
        }

        /// <summary>
        /// 判断是否为手机号码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsMobilePhone(string input)
        {
            if (string.IsNullOrEmpty(input)) return false;
            Regex regex = new Regex("^1\\d{10}$");
            return regex.IsMatch(input);
        }
    }
}
