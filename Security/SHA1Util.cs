﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace IEMSOFT.Foundation.Security
{
    class SHA1Util
    {
        public static String getSha1(String str,string charset)
        {
            //建立SHA1对象
            SHA1 sha = new SHA1CryptoServiceProvider();
            //将mystr转换成byte[] 
            Encoding enc = Encoding.GetEncoding(charset);
            byte[] dataToHash = enc.GetBytes(str);
            //Hash运算
            byte[] dataHashed = sha.ComputeHash(dataToHash);
            //将运算结果转换成string
            string hash = BitConverter.ToString(dataHashed).Replace("-", "");
            return hash;
        }
    }
}