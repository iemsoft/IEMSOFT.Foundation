using System;
using System.Security.Cryptography;
using System.Text;

namespace IEMSOFT.Foundation.Security
{
	public class MD5Util
	{
		public MD5Util()
		{
		}

		public static string GetMD5(string encypStr, string charset)
		{
			string retStr;
			MD5CryptoServiceProvider m5 = new MD5CryptoServiceProvider();
			byte[] inputBye;
			byte[] outputBye;
			try
			{
                inputBye = Encoding.GetEncoding(charset).GetBytes(encypStr);
			}
			catch (Exception )
			{
				inputBye = Encoding.GetEncoding("GB2312").GetBytes(encypStr);
			}
			outputBye = m5.ComputeHash(inputBye);

			retStr = System.BitConverter.ToString(outputBye);
			retStr = retStr.Replace("-", "").ToUpper();
			return retStr;
		}
	}
}
